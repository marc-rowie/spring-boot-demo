package com.example.demo.service;

import com.example.demo.model.Student;

import java.util.List;

public interface StudentService {
    List<Student> getStudents();

    void registerStudent(Student student);

    void deleteStudent(Long id);

    void updateStudent(Long id, String name, String email);
}
