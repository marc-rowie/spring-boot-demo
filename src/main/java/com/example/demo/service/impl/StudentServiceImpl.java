package com.example.demo.service.impl;

import com.example.demo.repository.StudentRepository;
import com.example.demo.service.StudentService;
import com.example.demo.model.Student;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class StudentServiceImpl implements StudentService {

    private final StudentRepository studentRepository;

    public List<Student> getStudents() {
        return studentRepository.findAll();
    }

    public void registerStudent(Student student) {
        studentRepository.save(student);
    }

    public void deleteStudent(Long id) {
        boolean exists = studentRepository.existsById(id);
        if (!exists) {
            throw new IllegalStateException("Student does not exists");
        }
        else {
            Optional<Student> student = studentRepository.findById(id);
            studentRepository.delete(student.get());
        }
    }

    @Transactional
    public void updateStudent(Long id, String name, String email) {
        boolean exists = studentRepository.existsById(id);
        if (!exists) {
            throw new IllegalStateException("Student does not exists");
        }
        else {
            Student student = studentRepository.findById(id).get();
            student.setName(name);
            student.setEmail(email);
        }
    }
}
