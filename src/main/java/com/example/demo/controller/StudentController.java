package com.example.demo.controller;

import com.example.demo.service.StudentService;
import com.example.demo.model.Student;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "api/v1/student")
@RequiredArgsConstructor
public class StudentController {

    private final StudentService studentService;

//    @Autowired //Another way of doing @RequiredArgsConstructor
//    public StudentController(StudentService studentService) {
//        this.studentService = studentService;
//    }

    @GetMapping
    public List<Student> getStudents() {
        return studentService.getStudents();
    }

    @PostMapping
    public void registerStudent(@RequestBody Student student) {
        studentService.registerStudent(student);
    }

    @DeleteMapping
    public void deleteStudent(@RequestParam Long id) {
        studentService.deleteStudent(id);
    }

    @PutMapping
    public void updateStudent(@RequestParam Long id, @RequestParam String name, @RequestParam String email) {
        studentService.updateStudent(id, name, email);
    }
}
